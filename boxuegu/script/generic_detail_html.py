#!/usr/bin/env python
import sys
from django.shortcuts import render
sys.path.insert(0, '../')
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "boxuegu.settings")
import django
django.setup()
from django.template import loader
from django.conf import settings
from courses.models import Course
from courses import models


def generate_detail_html(course_id):
    course = Course.objects.get(pk=course_id)
    # 上下文
    context = {
            'course': course,
            'MEDIA_URL': settings.MEDIA_URL,
        }


    template = loader.get_template('course-detail.html')
    html_text = template.render(context)
    file_path = os.path.join(settings.STATICFILES_DIRS[0], 'courses/'+str(course_id)+'.html')
    with open(file_path, 'w') as f:
        f.write(html_text)

if __name__ == '__main__':
    courses = models.Course.objects.all()
    for course in courses:
        print(course.id)
        generate_detail_html(course.id)
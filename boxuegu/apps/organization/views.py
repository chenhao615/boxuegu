import json
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from courses.models import Course
from operation.models import UserFavorite


class TeacherList(View):
    pass


class OrgList(View):
    pass


class OrgTeacher(View):
    pass


#取消收藏
class DelFav(View):
    def post(self,request):
        # 需要ajax传递课程id,userid
        user_id=request.user.id
        fav_type=request.POST.get('fav_type')
        fav_id=request.POST.get('fav_id')
        course=UserFavorite.objects.filter(user_id=user_id,fav_type=fav_type,fav_id=fav_id).delete()
        return JsonResponse({'status':'OK'})

# 点击收藏
class AddFav(View):
    def post(self, request):
        # 得到course信息
        # data = json.loads(request.body.decode())
        data = request.POST
        fav_id = data.get('fav_id')
        fav_type = data.get('fav_type')
        if not request.user.is_authenticated():
            return JsonResponse({'status': 'fail', 'msg': '用户未登录'})
        user_id = request.user.id

        # 保存到数据库
        UserFavorite.objects.create(
            fav_id=fav_id,
            fav_type=fav_type,
            user_id=user_id,
        )
        # 返回响应数据
        return JsonResponse({'status': 'success', 'msg': 'OK','has_fav_course':'1'})


class TeacherDetail(View):
    def get(self, request, pk):
        # 根据课程ID查询课程描述

        return render(request, 'teacher-detail.html')


class OrgHome(View):
    def get(self, request, pk):
        # 根据课程ID查询课程描述

        return render(request, 'org-detail-homepage.html')

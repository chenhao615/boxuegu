from django.conf.urls import url, include

from organization import views


urlpatterns = [

    url(r'^organization/org/$', views.OrgList.as_view(), name='org_list'),
    url(r'^organization/list/$', views.TeacherList.as_view(), name='teacher_list'),
    url(r'^teacher/detail/(\d+)/$', views.TeacherDetail.as_view(), name='teacher_detail'),
    url(r'^organization/teacher/$', views.OrgTeacher.as_view(), name='org_teacher'),
    url(r'^add/fav/$', views.AddFav.as_view(), name='add_fav'),
    url(r'^organization/home/(\d+)/$', views.OrgHome.as_view(), name='org_home'),
    url(r'^organization/delfav/$', views.DelFav.as_view(), name='del_fav'),
]
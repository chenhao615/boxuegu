from django.conf.urls import url, include

from courses import views


from courses.views import CourseDetail, CourseList


urlpatterns = [
    url(r'^course/detail/(\d+)/$', views.CourseDetail.as_view(), name='course_detail'),

    # url(r'^org/detail/(\d+)/$', views.OrgDetail.as_view(), name='org_detail'),
    url(r'^course/list/$', views.CourseList.as_view(), name='course_list'),
    url(r'^course/info/(\d+)/$', views.CourseInfo.as_view(), name='course_info'),
    # url(r'^teacher/detail/$', views.TeacherDetail.as_view(), name='teacher_detail'),

]

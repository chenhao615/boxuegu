from django.shortcuts import render
from django.views import View

from boxuegu import settings
from courses.models import Course
from operation.models import UserFavorite


# 课程详情页
class CourseDetail(View):
    def get(self, request, pk):

        # 根据课程ID查询课程描述
        course = Course.objects.get(pk=pk)
        if not course:
            return render(request, '404.html')

        # 查询数据库判断has fav course是否存在
        user = request.user

        context = {
            'course': course,
            'MEDIA_URL': settings.MEDIA_URL,
        }
        has_fav_course=''
        has_fav_org=''
        if user.is_authenticated():
            # if UserFavorite.objects.filter(fav_id=pk, fav_type=1,user=user.id).count() == 0:
            if UserFavorite.objects.filter(fav_id=pk, fav_type=1, user=user.id):
                has_fav_course = 'ok'
            if UserFavorite.objects.filter(fav_id=pk, fav_type=2, user=user.id):
                has_fav_org = 'ok'
            if not has_fav_course is None:
                context['has_fav_course']=has_fav_course
            if not has_fav_org is None:
                context['has_fav_org']=has_fav_org

            return render(request, 'course-detail.html', context)
        else:
            # 响应course对象
            return render(request, 'course-detail.html', context)


# class OrgDetail(View):
#     def get(self, request, pk):
#         # 根据课程ID查询课程描述
#         Course.objects.get()
#         return render(request, 'org-detail.html')


class CourseList(View):

    def get(self,request):
        courses = Course.objects.all()
        context = {
            'all_courses': courses,
            'MEDIA_URL': settings.MEDIA_URL
        }
        return render(request, 'course-list.html', context)


class CourseInfo(View):
    def get(self, request, pk):
        return render(request, 'course-play.html')
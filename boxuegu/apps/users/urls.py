from django.conf.urls import url
from apps.users import views


urlpatterns = [
    # 首页
    url(r'^$', views.IndexView.as_view(), name="index"),
    # 登出
    url(r'^logout/$', views.LogoutView.as_view(), name="logout"),
    # 路由名为register
    url(r'^register/$', views.UserRegister.as_view(), name='register'),
    # login
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    # course_detail
    url(r'^modify_pwd/$', views.ModifypwdView.as_view(), name="modify_pwd"),
    url(r'^reset/(?P<active_code>.*)/$', views.ResetView.as_view(), name='reset_pwd'),
    url(r'^center/$', views.UserCenterView.as_view(), name='user_center'),
    url(r'^users/info/$', views.UserInfoView.as_view(), name='user_info'),
    url(r'^image/upload/$', views.UploadImageView.as_view(), name='image_upload'),
    url(r'^mycourse/$', views.MyCourseView.as_view(), name='mycourse'),
    # url(r'^course/detail/$', views.CourseDetail.as_view(), name='course_detail'),
    # url(r'^teacher/detail/$', views.TeacherDetail.as_view(), name='teacher_detail'),
    url(r'^myfav/org/$', views.MyFavOrgView.as_view(), name='myfav_org'),
    url(r'^mycourse/$', views.MyCourseView.as_view(), name='mycourse'),
    url(r'^mymessage/$', views.MyMessageView.as_view(), name='mymessage'),
    url(r'^myfav/teacher/$', views.MyFavTeacherView.as_view(), name='myfav_teacher'),
    url(r'^myfav/course/$', views.MyFavCourseView.as_view(), name='myfav_course'),
    url(r'^forget/pwd/$', views.ForgetPwdView.as_view(), name='forget_pwd'),
    url(r'^user_pwd/$',views.UserInfoPWD.as_view(), name='user_pwd')
]

